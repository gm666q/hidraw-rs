use core::ffi::{c_char, c_int, c_void};

use ioctl::ioctl;

use crate::{
	hidraw_devinfo, hidraw_report_descriptor, HIDIOCGFEATURE, HIDIOCGINPUT, HIDIOCGOUTPUT, HIDIOCGRAWINFO,
	HIDIOCGRAWNAME, HIDIOCGRAWPHYS, HIDIOCGRAWUNIQ, HIDIOCGRDESC, HIDIOCGRDESCSIZE, HIDIOCSFEATURE, HIDIOCSINPUT,
	HIDIOCSOUTPUT,
};

#[inline]
pub unsafe fn hidiocgrdescsize(fd: c_int, data: *mut c_int) -> c_int {
	ioctl(fd, HIDIOCGRDESCSIZE!(), data)
}

#[inline]
pub unsafe fn hidiocgrdesc(fd: c_int, data: *mut hidraw_report_descriptor) -> c_int {
	ioctl(fd, HIDIOCGRDESC!(), data)
}

#[inline]
pub unsafe fn hidiocgrawinfo(fd: c_int, data: *mut hidraw_devinfo) -> c_int {
	ioctl(fd, HIDIOCGRAWINFO!(), data)
}

#[inline]
pub unsafe fn hidiocgrawname(fd: c_int, data: *mut c_char, len: usize) -> c_int {
	ioctl(fd, HIDIOCGRAWNAME!(len), data)
}

#[inline]
pub unsafe fn hidiocgrawphys(fd: c_int, data: *mut c_char, len: usize) -> c_int {
	ioctl(fd, HIDIOCGRAWPHYS!(len), data)
}

#[inline]
pub unsafe fn hidiocsfeature(fd: c_int, data: *const c_void, len: usize) -> c_int {
	ioctl(fd, HIDIOCSFEATURE!(len), data)
}

#[inline]
pub unsafe fn hidiocgfeature(fd: c_int, data: *mut c_void, len: usize) -> c_int {
	ioctl(fd, HIDIOCGFEATURE!(len), data)
}

#[inline]
pub unsafe fn hidiocgrawuniq(fd: c_int, data: *mut c_char, len: usize) -> c_int {
	ioctl(fd, HIDIOCGRAWUNIQ!(len), data)
}

#[inline]
pub unsafe fn hidiocsinput(fd: c_int, data: *const c_void, len: usize) -> c_int {
	ioctl(fd, HIDIOCSINPUT!(len), data)
}

#[inline]
pub unsafe fn hidiocginput(fd: c_int, data: *mut c_void, len: usize) -> c_int {
	ioctl(fd, HIDIOCGINPUT!(len), data)
}

#[inline]
pub unsafe fn hidiocsoutput(fd: c_int, data: *const c_void, len: usize) -> c_int {
	ioctl(fd, HIDIOCSOUTPUT!(len), data)
}

#[inline]
pub unsafe fn hidiocgoutput(fd: c_int, data: *mut c_void, len: usize) -> c_int {
	ioctl(fd, HIDIOCGOUTPUT!(len), data)
}
