#![allow(clippy::missing_safety_doc, non_camel_case_types, non_snake_case)]
#![cfg_attr(docsrs, feature(doc_cfg))]
#![cfg(any(target_os = "android", target_os = "linux"))]
#![no_std]

extern crate ioctl_macro as ioctl;

use self::__private::{__s16, __u32, __u8};
#[cfg(feature = "wrappers")]
#[cfg_attr(docsrs, doc(cfg(feature = "wrappers")))]
pub use self::wrappers::*;

#[doc(hidden)]
pub mod __private {
	pub use ioctl::{_IOC, _IOC_READ, _IOC_WRITE, _IOR};

	pub type __s16 = core::ffi::c_short;
	pub type __u8 = core::ffi::c_uchar;
	pub type __u32 = core::ffi::c_uint;
}
#[cfg(feature = "wrappers")]
#[cfg_attr(docsrs, doc(cfg(feature = "wrappers")))]
mod wrappers;

macro_rules! HID_MAX_DESCRIPTOR_SIZE {
	() => {
		4096
	};
}

#[cfg_attr(feature = "extra_traits", derive(Debug, Eq, Hash, PartialEq))]
#[derive(Clone, Copy)]
#[repr(C)]
pub struct hidraw_report_descriptor {
	pub size: __u32,
	pub value: [__u8; HID_MAX_DESCRIPTOR_SIZE!()],
}

#[cfg_attr(feature = "extra_traits", derive(Debug, Eq, Hash, PartialEq))]
#[derive(Clone, Copy)]
#[repr(C)]
pub struct hidraw_devinfo {
	pub bustype: __u32,
	pub vendor: __s16,
	pub product: __s16,
}

#[macro_export]
macro_rules! HIDIOCGRDESCSIZE {
	() => {
		$crate::__private::_IOR!(b'H', 0x01, core::ffi::c_int)
	};
}
#[macro_export]
macro_rules! HIDIOCGRDESC {
	() => {
		$crate::__private::_IOR!(b'H', 0x02, $crate::hidraw_report_descriptor)
	};
}
#[macro_export]
macro_rules! HIDIOCGRAWINFO {
	() => {
		$crate::__private::_IOR!(b'H', 0x03, $crate::hidraw_devinfo)
	};
}
#[macro_export]
macro_rules! HIDIOCGRAWNAME {
	($len:expr) => {
		$crate::__private::_IOC!($crate::__private::_IOC_READ!(), b'H', 0x04, $len)
	};
}
#[macro_export]
macro_rules! HIDIOCGRAWPHYS {
	($len:expr) => {
		$crate::__private::_IOC!($crate::__private::_IOC_READ!(), b'H', 0x05, $len)
	};
}
#[macro_export]
macro_rules! HIDIOCSFEATURE {
	($len:expr) => {
		$crate::__private::_IOC!(
			$crate::__private::_IOC_WRITE!() | $crate::__private::_IOC_READ!(),
			b'H',
			0x06,
			$len
		)
	};
}
#[macro_export]
macro_rules! HIDIOCGFEATURE {
	($len:expr) => {
		$crate::__private::_IOC!(
			$crate::__private::_IOC_WRITE!() | $crate::__private::_IOC_READ!(),
			b'H',
			0x07,
			$len
		)
	};
}
#[macro_export]
macro_rules! HIDIOCGRAWUNIQ {
	($len:expr) => {
		$crate::__private::_IOC!($crate::__private::_IOC_READ!(), b'H', 0x08, $len)
	};
}
#[macro_export]
macro_rules! HIDIOCSINPUT {
	($len:expr) => {
		$crate::__private::_IOC!(
			$crate::__private::_IOC_WRITE!() | $crate::__private::_IOC_READ!(),
			b'H',
			0x09,
			$len
		)
	};
}
#[macro_export]
macro_rules! HIDIOCGINPUT {
	($len:expr) => {
		$crate::__private::_IOC!(
			$crate::__private::_IOC_WRITE!() | $crate::__private::_IOC_READ!(),
			b'H',
			0x0A,
			$len
		)
	};
}
#[macro_export]
macro_rules! HIDIOCSOUTPUT {
	($len:expr) => {
		$crate::__private::_IOC!(
			$crate::__private::_IOC_WRITE!() | $crate::__private::_IOC_READ!(),
			b'H',
			0x0B,
			$len
		)
	};
}
#[macro_export]
macro_rules! HIDIOCGOUTPUT {
	($len:expr) => {
		$crate::__private::_IOC!(
			$crate::__private::_IOC_WRITE!() | $crate::__private::_IOC_READ!(),
			b'H',
			0x0C,
			$len
		)
	};
}

#[macro_export]
macro_rules! HIDRAW_FIRST_MINOR {
	() => {
		0
	};
}
#[macro_export]
macro_rules! HIDRAW_MAX_DEVICES {
	() => {
		64
	};
}
#[macro_export]
macro_rules! HIDRAW_BUFFER_SIZE {
	() => {
		64
	};
}
