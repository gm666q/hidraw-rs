# hidraw-sys

This crate contains Linux hidraw definitions from `linux/hidraw.h`.

Those are raw definitions so for documentation see the official
[kernel documentation](https://www.kernel.org/doc/html/latest/hid/hidraw.html#the-hidraw-api).
