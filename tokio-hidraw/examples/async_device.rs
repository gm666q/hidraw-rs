use futures_util::future::ready;
use tokio::signal;
use tokio_hidraw::AsyncDevice;

async fn run() {
	let device = AsyncDevice::open("/dev/hidraw0").expect("Couldn't open a device");

	/*device
		.for_each(|event| {
			if let Ok(event) = event {
				println!("{event:?}");
			}
			ready(())
		})
		.await*/
}

#[tokio::main]
async fn main() {
	tokio::spawn(run());

	match signal::ctrl_c().await {
		Ok(()) => {}
		Err(err) => {
			eprintln!("Unable to listen for shutdown signal: {}", err);
		}
	}
}
