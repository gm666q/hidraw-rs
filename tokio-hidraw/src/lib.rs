#![cfg(any(target_os = "android", target_os = "linux"))]
//#![deny(missing_docs)]

use std::io;
use std::path::Path;
use std::pin::Pin;
use std::sync::Mutex;
use std::task::{Context, Poll};
use futures_core::Stream;

use tokio::io::unix::AsyncFd;

pub use hidraw::{RawInfo};

pub struct AsyncDevice {
	inner: Mutex<Inner>,
}

impl AsyncDevice {
	pub fn open<P: AsRef<Path>>(path: P) -> Result<Self, io::Error> {
		Ok(Self {
			inner: Mutex::new(Inner::new(hidraw::Device::open(path)?)?),
		})
	}
}

/*impl<T> Stream for AsyncDevice {
	type Item = Result<T, io::Error>;

	fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
		self.inner.lock().unwrap().poll_receive(cx)
	}
}*/

struct Inner {
	fd: AsyncFd<hidraw::Device>,
}

impl Inner {
	fn new(device: hidraw::Device) -> io::Result<Inner> {
		Ok(Inner {
			fd: AsyncFd::new(device)?,
		})
	}

	fn poll_receive<T>(&mut self, cx: &mut Context) -> Poll<Option<Result<T, io::Error>>> {
		match self.fd.poll_read_ready(cx) {
			Poll::Ready(Ok(mut ready_guard)) => {
				ready_guard.clear_ready();
				Poll::Ready(self.fd.get_mut().get_input_report_read(0).ok())
			}
			Poll::Ready(Err(err)) => Poll::Ready(Some(Err(err))),
			Poll::Pending => Poll::Pending,
		}
	}
}
