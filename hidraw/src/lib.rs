#![cfg_attr(docsrs, feature(doc_cfg))]
#![cfg(any(target_os = "android", target_os = "linux"))]
//#![deny(missing_docs)]

extern crate hidraw_sys as sys;

use std::io;

pub use self::device::Device;
pub use self::raw_info::RawInfo;

mod device;
pub mod raw_info;

pub type Result<T> = std::result::Result<T, io::Error>;
