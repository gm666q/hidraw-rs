#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
#[non_exhaustive]
#[repr(u32)]
pub enum BusType {
	Usb = 0x03,
	Hil = 0x04,
	Bluetooth = 0x05,
	Virtual = 0x06,
}
