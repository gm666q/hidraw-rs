use std::fmt;
use std::mem::transmute;

use sys::hidraw_devinfo;

use super::BusType;

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
#[repr(transparent)]
pub struct RawInfo(pub(crate) hidraw_devinfo);

impl RawInfo {
	pub const fn bus_type(&self) -> BusType {
		unsafe { transmute(self.0.bustype) }
	}

	pub const fn product(&self) -> i16 {
		self.0.product
	}

	pub const fn vendor(&self) -> i16 {
		self.0.vendor
	}
}

impl fmt::Debug for RawInfo {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		f.debug_struct("RawInfo")
			.field("bustype", &self.bus_type())
			.field("vendor", &format_args!("{:04x}", self.vendor()))
			.field("product", &format_args!("{:04x}", self.product()))
			.finish()
	}
}
