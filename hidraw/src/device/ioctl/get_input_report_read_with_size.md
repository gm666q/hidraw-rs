Request an input report using read with specific size.

# Safety

Calling this function with `size > size_of::<T>()` is undefined behaviour.
