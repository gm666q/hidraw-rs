Request an output report using HIDIOCGOUTPUT ioctl with specific size.

# Safety

Calling this function with `size > size_of::<T>()` is undefined behaviour.
