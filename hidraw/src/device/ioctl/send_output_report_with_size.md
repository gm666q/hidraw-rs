Send an output report using HIDIOCSOUTPUT ioctl with specific size.

# Safety

Calling this function with `size > size_of::<T>()` is undefined behaviour.
