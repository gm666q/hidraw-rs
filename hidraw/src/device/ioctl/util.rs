use std::io;

pub trait IsMinusOne {
	fn is_minus_one(&self) -> bool;
}

macro_rules! impl_is_minus_one {
	($($t:ident)*) => ($(impl IsMinusOne for $t {
		#[inline]
		fn is_minus_one(&self) -> bool {
			*self == -1
		}
	})*)
}

impl_is_minus_one! { i8 i16 i32 i64 i128 isize }

pub fn cvt<T: IsMinusOne>(t: T) -> io::Result<T> {
	if t.is_minus_one() {
		Err(io::Error::last_os_error())
	} else {
		Ok(t)
	}
}
