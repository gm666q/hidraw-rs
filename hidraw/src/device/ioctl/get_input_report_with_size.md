Request an input report using HIDIOCGINPUT ioctl with specific size.

# Safety

Calling this function with `size > size_of::<T>()` is undefined behaviour.
