Send an input report using HIDIOCSINPUT ioctl with specific size.

# Safety

Calling this function with `size > size_of::<T>()` is undefined behaviour.
