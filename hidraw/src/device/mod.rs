pub use self::device::Device;

#[allow(clippy::module_inception)]
mod device;
mod ioctl;
