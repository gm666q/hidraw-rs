use std::path::Path;

use hidraw::{Device, Result};

fn print_info<P: AsRef<Path>>(path: P) -> Result<()> {
	let mut device = Device::open(path)?;

	println!("{device:#?}");
	println!("\t{:?}", device.get_physical_address()?);
	println!("\t{:?}", device.get_report_descriptor()?);
	println!("\t{:?}", device.get_raw_unique()?);

	Ok(())
}

fn main() {
	for i in 0..20 {
		if let Err(error) = print_info(format!("/dev/hidraw{i}")) {
			eprintln!("{error}");
		}
	}
}
